import sys
import pygame


from pygame.locals import * 


WIDTH = 320
HEIGHT = 180


if __name__=="__main__":
    pygame.init()


    pygame.display.set_caption('pgz')
    # init window
    window_size = (WIDTH, HEIGHT)
    screen = pygame.display.set_mode(window_size, 0, 32)

    bg_surface = pygame.image.load('gfx/bg.png').convert()

    player_surface = pygame.image.load('gfx/gbWiz.png').convert()
    player_pos_x = WIDTH/2
    player_pos_y = HEIGHT/2

    running = True
    while(running): #game loop
        for ev in pygame.event.get():
            if(ev.type == pygame.QUIT): 
                running = False

        screen.blit(bg_surface, (0, 0))
        screen.blit(player_surface, (player_pos_x, player_pos_y))
        pygame.display.update()

    pygame.quit()
    sys.exit()